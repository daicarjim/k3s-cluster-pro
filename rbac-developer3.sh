#!/bin/bash

#Then the Operator resend files kubeconfig and YOURNAME.crt.
#Change YOURNAME for your nick or name initial. Remember authorize file with chmod +x NAMEFILE and then execute bash with ./NAMEFILE
# Apply commands:
export KUBECONFIG=$PWD/kubeconfig
kubectl config set-credentials YOURNAME \
  --client-key=$PWD/YOURNAME.key \
  --embed-certs=true

#NOTE: If then of execute bash show you issue or not connect your cluster, execute commands manually. 
